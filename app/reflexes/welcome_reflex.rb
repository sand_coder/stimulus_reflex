class WelcomeReflex < StimulusReflex::Reflex
    def greetings
        @hello = "Hello from Stimuls Reflex"
    end

    def increment
        @count = element.dataset[:count].to_i + element.dataset[:step].to_i
    end

    def say_hello
        @value = element.attributes[:value]
        @name = "Hello #{ @value }!"
    end

    def from_stimulus(step)
        puts session[:count].to_i
        puts session[:count].present?
        puts step
        session[:count] = session[:count].to_i + step.to_i
        @increment = session[:count]
    end
end